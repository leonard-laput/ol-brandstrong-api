const _ = require('lodash');

const TOKEN_TTL = 86400;

const AuthHelper = {
	handleSuccessfulAuth: async (ctx, user) => {
		const { redis, helpers } = ctx.custom;
		const { generateRandomString } = helpers.general;
		const { generateResponse } = helpers.response;

		// Step 1: Create token
		const token = generateRandomString();

		// Step 2: Save token to redis
		const key = `auth:token:${token}`;
		await redis.set(key, JSON.stringify(user));
		await redis.expire(key, TOKEN_TTL);
		await redis.sadd(`auth:user:${user._id}`, token);

		// Step 3: Return token and user
		const data = {
			auth: {
				token
			},
			user
		};
		return generateResponse(200, 'Authentication successful', data);
	},

	handleSuccessfulLogout: async (ctx, token, user) => {
		const { redis, helpers } = ctx.custom;
		const { generateResponse } = helpers.response;

		const smemberKey = `auth:user:${user._id}`;
		const key = `auth:token:${token}`;
		await redis.del(key);
		await redis.srem(smemberKey, token);

		return generateResponse(200, 'Successfully logged out.');
	},

	handleValidateUserToken: async (ctx, token) => {
		const { redis, helpers } = ctx.custom;
		const { generateResponse, generateErrorResponse } = helpers.response;

		const key = `auth:token:${token}`;
		const exists = await redis.exists(key);

		return exists ? generateResponse(200, 'Token valid.') : generateErrorResponse(401, 'Token invalid.');
	},

	deleteAllUserTokenFromRedis: async (redis, user_id) => {
		const smemberKey = `auth:user:${user_id}`;
		const tokens = await redis.smembers(smemberKey);
		await _.map(tokens, async token => {
			const key = `auth:token:${token}`;
			await redis.del(key);
		});
		await redis.del(smemberKey);
	}
};

module.exports = AuthHelper;

const Router = require('koa-router');
const appRoot = require('app-root-path');
const requireAll = require('require-all');

const controllers = requireAll(`${appRoot}/app/controllers`);
const middlewares = requireAll(`${appRoot}/app/middlewares`);

const router = new Router({
	prefix: '/v1/comments'
});

// middleware
// router.use(middlewares.check_token);

// GET /v1/comments
router.get('/', middlewares.ajv, controllers.v1.comments.getComments);

// POST /v1/comments
router.post('/', middlewares.ajv, middlewares.check_token, controllers.v1.comments.createComment);

// PATCH /v1/comments/:id
router.patch('/:id', middlewares.ajv, middlewares.check_token, controllers.v1.comments.updateCommentById);

module.exports = router;

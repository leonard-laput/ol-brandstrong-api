const Router = require('koa-router');
const appRoot = require('app-root-path');
const requireAll = require('require-all');

const controllers = requireAll(`${appRoot}/app/controllers`);
const middlewares = requireAll(`${appRoot}/app/middlewares`);

const router = new Router({
	prefix: '/v1/users'
});

// middleware
router.use(middlewares.check_token);

// GET /v1/users/:id
router.get('/:id', middlewares.ajv, controllers.v1.users.getUserById);

// PATCH /v1/users/:id
router.patch('/:id', middlewares.ajv, controllers.v1.users.updateUserById);

module.exports = router;

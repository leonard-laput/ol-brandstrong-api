const Router = require('koa-router');
const appRoot = require('app-root-path');
const requireAll = require('require-all');

const controllers = requireAll(`${appRoot}/app/controllers`);
const middlewares = requireAll(`${appRoot}/app/middlewares`);

const router = new Router({
	prefix: '/v1/auth'
});

// POST /v1/auth/login
router.post('/login', middlewares.ajv, controllers.v1.auth.login);

// POST /v1/auth/logout
router.post('/logout', middlewares.ajv, controllers.v1.auth.logout);

// POST /v1/auth/signup
router.post('/signup', middlewares.ajv, controllers.v1.auth.signup);

// POST /v1/auth/validate-user-token
router.post('/validate-user-token', middlewares.ajv, controllers.v1.auth.validateUserToken);

module.exports = router;

const Router = require('koa-router');
const appRoot = require('app-root-path');
const requireAll = require('require-all');

const controllers = requireAll(`${appRoot}/app/controllers`);
const middlewares = requireAll(`${appRoot}/app/middlewares`);

const router = new Router({
	prefix: '/v1/assets'
});

// GET /v1/assets
router.get('/', middlewares.ajv, controllers.v1.assets.getAssets);

// GET /v1/assets/:id
router.get('/:id', middlewares.ajv, controllers.v1.assets.getAssetById);

// POST /v1/assets
router.post('/', middlewares.ajv, middlewares.check_token, controllers.v1.assets.createAsset);

module.exports = router;

const Ajv = require('ajv');
const appRoot = require('app-root-path');

const requestSchema = require(`${appRoot}/app/schemas`);

function formatMatchedRoute(route) {
	if (route.substring(route.length - 1) === '/') {
		return route.substring(0, route.length - 1);
	}
	return route;
}

module.exports = async (ctx, next) => {
	const { generateErrorResponse } = ctx.custom.helpers.response;

	// find for the schema
	const matchedRoute = formatMatchedRoute(ctx._matchedRoute);
	const schemaKey = `${ctx.request.method} ${matchedRoute}`;
	const schema = requestSchema[schemaKey];

	// initialize ajv
	const ajv = new Ajv({
		removeAdditional: true,
		useDefaults: true,
		coerceTypes: true
	});

	const data = {
		params: ctx.params,
		query: ctx.request.query,
		body: ctx.request.body
	};

	// validate data
	const validate = ajv.compile(schema);
	const valid = validate(data);

	if (!valid) {
		console.error('[!] Schema validation error [!]');
		console.error('[!] ======================= [!]');
		console.error('data => ', data);
		console.error(validate.errors);
		console.error('[!] ======================= [!]');

		ctx.body = generateErrorResponse(4034, `Invalid request - ${JSON.stringify(validate.errors)}`);
		return;
	}

	await next();
};

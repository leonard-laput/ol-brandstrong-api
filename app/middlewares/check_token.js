const _ = require('lodash');

module.exports = async (ctx, next) => {
	const { helpers, models } = ctx.custom;
	const User = models.user;
	const { getBearerToken } = helpers.headers;
	const { generateResponse } = helpers.response;

	const authorizationToken = getBearerToken(ctx);

	if (_.isNil(authorizationToken)) {
		ctx.body = generateResponse(4031, 'Missing Bearer Token.');
		return;
	}

	const redisKey = `auth:token:${authorizationToken}`;
	const userString = await ctx.custom.redis.get(redisKey);
	if (_.isNil(userString)) {
		ctx.body = generateResponse(4032, 'Invalid Bearer Token.');
		return;
	}

	const jsonUser = JSON.parse(userString);
	const dbUser = await User.findById(jsonUser._id);

	// Set user to context
	_.set(ctx, 'custom.user', dbUser);
	await next();
};

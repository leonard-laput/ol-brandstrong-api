const appRoot = require('app-root-path');
const requireAll = require('require-all');

const schemas = requireAll(`${appRoot}/app/schemas`);

module.exports = {
	// /v1/auth
	'POST /v1/auth/login': schemas.v1.auth.login,
	'POST /v1/auth/logout': schemas.v1.auth.logout,
	'POST /v1/auth/signup': schemas.v1.auth.signup,
	'POST /v1/auth/validate-user-token': schemas.v1.auth.validateUserToken,

	// /v1/users
	'GET /v1/users/:id': schemas.v1.users.getUserById,
	'PATCH /v1/users/:id': schemas.v1.users.updateUserById,

	// /v1/assets
	'GET /v1/assets': schemas.v1.assets.getAssets,
	'GET /v1/assets/:id': schemas.v1.assets.getAssetById,
	'POST /v1/assets': schemas.v1.assets.createAsset,

	// /v1/comments
	'GET /v1/comments': schemas.v1.comments.getComments,
	'POST /v1/comments': schemas.v1.comments.createComment,
	'PATCH /v1/comments/:id': schemas.v1.comments.updateCommentById
};

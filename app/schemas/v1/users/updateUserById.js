module.exports = {
	properties: {
		params: {
			type: 'object',
			properties: {
				id: { type: 'string', minLength: 24, maxLength: 24 }
			},
			required: ['id'],
			additionalProperties: false
		},
		body: {
			type: 'object',
			properties: {
				user: {
					type: 'object',
					properties: {
						email: { type: 'string', format: 'email', transform: ['trim', 'toLowerCase'] },
						username: { type: 'string' },
						password: { type: 'string' },
						name: { type: 'string' },
						country: { type: 'string' },
						state: { type: 'string' },
						city: { type: 'string' },
						avatar_url: { type: 'string' }
					}
				}
			},
			required: ['user'],
			additionalProperties: false
		}
	},
	additionalProperties: false
};

module.exports = {
	properties: {
		query: {
			type: 'object',
			properties: {
				is_media: { type: 'boolean', default: false },
				is_avatar: { type: 'boolean', default: false },
				user: { type: 'string', minLength: 24, maxLength: 24 }
			},
			additionalProperties: false
		}
	},
	additionalProperties: false
};

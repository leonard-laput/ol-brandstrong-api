module.exports = {
	properties: {
		body: {
			type: 'object',
			properties: {
				asset: {
					type: 'object',
					properties: {
						file_name: { type: 'string' },
						file: { type: 'string' },
						is_media: { type: 'boolean' },
						is_avatar: { type: 'boolean' }
					},
					required: ['file_name', 'file'],
					additionalProperties: false
				}
			},
			required: ['asset'],
			additionalProperties: false
		}
	},
	additionalProperties: false
};

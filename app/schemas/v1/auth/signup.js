module.exports = {
	properties: {
		body: {
			type: 'object',
			properties: {
				email: { type: 'string', format: 'email', transform: ['trim', 'toLowerCase'] },
				username: { type: 'string' },
				password: { type: 'string' },
				name: { type: 'string' },
				country: { type: 'string' },
				state: { type: 'string' },
				city: { type: 'string' }
			},
			required: ['email', 'username', 'password', 'name'],
			additionalProperties: false
		}
	},
	additionalProperties: false
};

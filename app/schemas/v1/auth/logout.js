module.exports = {
	properties: {
		body: {
			type: 'object',
			properties: {
				token: { type: 'string', minLength: 20, maxLength: 20 }
			},
			required: ['token'],
			additionalProperties: false
		}
	},
	additionalProperties: false
};

module.exports = {
	properties: {
		body: {
			type: 'object',
			properties: {
				comment: {
					type: 'object',
					properties: {
						text: { type: 'string', maxLength: 140 },
						asset: { type: 'string', minLength: 24, maxLength: 24 }
					},
					required: ['text', 'asset'],
					additionalProperties: false
				}
			},
			required: ['comment'],
			additionalProperties: false
		}
	},
	additionalProperties: false
};

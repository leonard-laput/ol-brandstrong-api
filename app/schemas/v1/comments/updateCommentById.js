module.exports = {
	properties: {
		params: {
			type: 'object',
			properties: {
				id: { type: 'string', minLength: 24, maxLength: 24 }
			},
			required: ['id'],
			additionalProperties: false
		},
		body: {
			type: 'object',
			properties: {
				comment: {
					type: 'object',
					properties: {
						text: { type: 'string', maxLength: 140 }
					}
				}
			},
			required: ['comment'],
			additionalProperties: false
		}
	},
	additionalProperties: false
};

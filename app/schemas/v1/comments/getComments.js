module.exports = {
	properties: {
		query: {
			type: 'object',
			properties: {
				asset: { type: 'string', minLength: 24, maxLength: 24 }
			},
			required: ['asset'],
			additionalProperties: false
		}
	},
	additionalProperties: false
};

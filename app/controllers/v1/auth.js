const _ = require('lodash');
const sha1 = require('sha1');

module.exports = {
	login: async ctx => {
		const { username, password } = ctx.request.body;
		const { models, helpers } = ctx.custom;
		const { generateResponse } = helpers.response;
		const { handleSuccessfulAuth } = helpers.auth;

		const User = models.user;
		const user = await User.findOne({
			username,
			password: sha1(password)
		});

		if (!user) {
			ctx.body = generateResponse(4041, 'Username or password is incorrect');
			return;
		}

		ctx.body = await handleSuccessfulAuth(ctx, user);
	},

	logout: async ctx => {
		const { token } = ctx.request.body;
		const { redis, helpers } = ctx.custom;
		const { generateErrorResponse } = helpers.response;
		const { handleSuccessfulLogout } = helpers.auth;

		const key = `auth:token:${token}`;
		const exists = await redis.exists(key);

		if (!exists) {
			ctx.body = generateErrorResponse(4041, 'Token invalid');
			return;
		}

		const user = JSON.parse(await redis.get(key));
		ctx.body = await handleSuccessfulLogout(ctx, token, user);
	},

	validateUserToken: async ctx => {
		const { token } = ctx.request.body;
		const { helpers } = ctx.custom;
		const { handleValidateUserToken } = helpers.auth;

		ctx.body = await handleValidateUserToken(ctx, token);
	},

	signup: async ctx => {
		const { models, helpers } = ctx.custom;
		const { exists } = helpers.existence;
		const { generateResponse, generateErrorResponse } = helpers.response;

		const { body: signupRequest } = ctx.request;
		const { email, username } = signupRequest;

		const User = models.user;
		const user = new User();

		// Check if email is existing
		if (await exists({ email }, User)) {
			ctx.body = generateErrorResponse(4041, 'Email exists');
			return;
		}

		// Check if username is existing
		if (await exists({ username }, User)) {
			ctx.body = generateErrorResponse(4041, 'Username exists');
			return;
		}

		_.each(signupRequest, (value, key) => {
			if (key === 'password') {
				user[key] = sha1(value);
			} else {
				user[key] = value;
			}
		});
		await user.save();

		ctx.body = generateResponse(200, 'Signup successful');
	}
};

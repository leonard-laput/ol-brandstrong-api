const appRoot = require('app-root-path');
const AWS = require('aws-sdk');
const BB = require('bluebird');
const _ = require('lodash');
const uuidv4 = require('uuid/v4');

const config = require(`${appRoot}/config`);

AWS.config.update({
	accessKeyId: config.brandstrong_api.s3.access_key_id,
	secretAccessKey: config.brandstrong_api.s3.secret_access_key
});

const s3 = new AWS.S3();
// const download = BB.promisify(s3.getObject, { context: s3 });
const upload = BB.promisify(s3.upload, { context: s3 });

module.exports = {
	getAssets: async ctx => {
		const { models, helpers } = ctx.custom;
		const { generateResponse } = helpers.response;

		const { is_media, is_avatar, user } = ctx.request.query;
		const query = {
			is_media,
			is_avatar
		};

		if (user) {
			query.user = user;
		}

		const Asset = models.asset;
		const assets = await Asset.find(query).populate({
			path: 'user',
			select: '_id name username'
		});

		ctx.body = generateResponse(200, 'Successfuly get assets', {
			assets
		});
	},

	getAssetById: async ctx => {
		const { id } = ctx.params;
		const { models, helpers } = ctx.custom;
		const { generateResponse, generateNotFoundResponse } = helpers.response;

		const Asset = models.asset;
		const asset = await Asset.findById(id).populate({
			path: 'user',
			select: '_id name username'
		});

		if (!asset) {
			ctx.body = generateNotFoundResponse('Asset');
			return;
		}

		ctx.body = generateResponse(200, 'Successfully get asset by id', { asset });
	},

	createAsset: async ctx => {
		const { asset: assetRequest } = ctx.request.body;
		const {
			user: { _id },
			models,
			helpers
		} = ctx.custom;
		const { generateResponse, generateNotFoundResponse } = helpers.response;
		const { exists } = helpers.existence;

		const User = models.user;
		const Asset = models.asset;
		const newAsset = new Asset();

		if (!(await exists({ _id }, User))) {
			ctx.body = generateNotFoundResponse('User');
			return;
		}

		const uuid = uuidv4();
		const Body = new Buffer(assetRequest.file.split(',')[1], 'base64'); // eslint-disable-line
		const Key = [uuid.substr(0, 2), uuid.substr(2, 2), `${uuid}-${assetRequest.file_name}`].join('/');
		const ContentType = assetRequest.file.split(';')[0].split(':')[1];

		const uploadResponse = await upload({
			Bucket: config.brandstrong_api.s3.bucket,
			Body,
			Key,
			ContentType,
			ContentEncoding: 'base64'
		});

		_.each(assetRequest, (value, key) => {
			if (key !== 'file') {
				// Do not include the base64 string
				newAsset[key] = value;
			}
		});
		newAsset.uuid = uuid;
		newAsset.user = _id;
		newAsset.s3_key = Key;
		newAsset.url = uploadResponse.Location;
		await newAsset.save();

		ctx.body = generateResponse(200, 'File uploaded successfully.', {
			asset: newAsset
		});
	}
};

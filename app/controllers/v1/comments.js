const _ = require('lodash');

module.exports = {
	getComments: async ctx => {
		const { models, helpers } = ctx.custom;
		const { generateResponse } = helpers.response;

		const { asset } = ctx.request.query;
		const query = { asset };

		const Comment = models.comment;
		const comments = await Comment.find(query)
			.populate({
				path: 'user',
				select: '_id name username avatar_url'
			})
			.sort({ created_at: -1 });

		ctx.body = generateResponse(200, 'Successfuly get comments', {
			comments
		});
	},

	createComment: async ctx => {
		const { user, models, helpers } = ctx.custom;
		const { generateResponse, generateNotFoundResponse } = helpers.response;

		const { comment: commentRequest } = ctx.request.body;
		const { asset: assetId } = commentRequest;

		const Comment = models.comment;
		const Asset = models.asset;
		const asset = await Asset.findById(assetId);

		if (!asset) {
			ctx.body = generateNotFoundResponse('Asset');
			return;
		}

		const comment = new Comment();
		comment.user = user._id;
		_.each(commentRequest, (value, key) => {
			comment[key] = value;
		});
		await comment.save();

		ctx.body = generateResponse(200, 'Successfully created comment', { comment });
	},

	updateCommentById: async ctx => {
		const { models, helpers } = ctx.custom;
		const { generateResponse, generateNotFoundResponse } = helpers.response;

		const { id } = ctx.params;
		const { comment: commentRequest } = ctx.request.body;

		const Comment = models.comment;
		const comment = await Comment.findById(id);

		if (!comment) {
			ctx.body = generateNotFoundResponse('Comment to be updated');
			return;
		}

		_.each(commentRequest, (value, key) => {
			comment[key] = value;
		});
		await comment.save();

		ctx.body = generateResponse(200, 'Successfully updated comment', { comment });
	}
};

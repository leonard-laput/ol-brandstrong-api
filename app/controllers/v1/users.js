const _ = require('lodash');
const sha1 = require('sha1');

module.exports = {
	getUserById: async ctx => {
		const { id } = ctx.params;
		const { models, helpers } = ctx.custom;
		const { generateResponse, generateNotFoundResponse } = helpers.response;

		const User = models.user;
		const user = await User.findById(id);

		if (!user) {
			ctx.body = generateNotFoundResponse('User');
			return;
		}

		ctx.body = generateResponse(200, 'Successfully get user by id', { user });
	},

	updateUserById: async ctx => {
		const { models, helpers } = ctx.custom;
		const { exists } = helpers.existence;
		const { generateResponse, generateNotFoundResponse } = helpers.response;

		const { id } = ctx.params;
		const { user: userRequest } = ctx.request.body;
		const { email, username } = userRequest;

		const User = models.user;
		const user = await User.findById(id);

		if (!user) {
			ctx.body = generateNotFoundResponse('User to be updated');
			return;
		}

		// Check if email is existing
		if (email && user.email && email !== user.email && (await exists({ email }, User))) {
			ctx.body = generateResponse(404, 'Email exists');
			return;
		}

		// Check if username is existing
		if (username && user.username && username !== user.username && (await exists({ username }, User))) {
			ctx.body = generateResponse(404, 'Username exists');
			return;
		}

		_.each(userRequest, (value, key) => {
			if (key === 'password') {
				user[key] = sha1(value);
			} else {
				user[key] = value;
			}
		});
		await user.save();

		ctx.body = generateResponse(200, 'Successfully updated profile', { user });
	}
};

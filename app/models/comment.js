const mongoose = require('mongoose');

const { Schema } = mongoose;

const schema = new Schema(
	{
		text: { type: String, required: true },
		asset: { type: Schema.Types.ObjectId, ref: 'Asset', required: true },
		user: { type: Schema.Types.ObjectId, ref: 'User', required: true }
	},
	{
		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
		collection: 'comment',
		versionKey: false
	}
);

module.exports = mongoose.model('Comment', schema);

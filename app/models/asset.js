const mongoose = require('mongoose');

const { Schema } = mongoose;

const schema = new Schema(
	{
		uuid: { type: String, unique: true },
		user: { type: Schema.Types.ObjectId, ref: 'User', required: true },
		file_name: { type: String },
		s3_key: { type: String },
		url: { type: String },

		is_media: { type: String, default: false },
		is_avatar: { type: String, default: false }
	},
	{
		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
		collection: 'asset',
		versionKey: false
	}
);

module.exports = mongoose.model('Asset', schema);

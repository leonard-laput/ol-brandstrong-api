const mongoose = require('mongoose');

const { Schema } = mongoose;

const schema = new Schema(
	{
		email: { type: String, required: true, unique: true, lowercase: true },
		username: { type: String, required: true, unique: true },
		password: { type: String, required: true },
		name: { type: String, required: true },
		country: { type: String },
		state: { type: String },
		city: { type: String },
		avatar_url: { type: String }
	},
	{
		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
		collection: 'user',
		versionKey: false
	}
);

module.exports = mongoose.model('User', schema);
